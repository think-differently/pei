function getAge(datestring) {
    var today = new Date();
    var birthDate = new Date(datestring);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getMonth() < birthDate.getMonth())) {
        age--;
    }
    return age;
}

if ($(window).width() < 768) {
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('header').addClass('changeColor')
      }
      if ($(this).scrollTop() < 50) {
        $('header').removeClass('changeColor')
      }
    });
  });
}