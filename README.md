[![pipeline status](https://gitlab.com/think-differently/pei/badges/master/pipeline.svg)](https://gitlab.com/think-differently/pei/commits/master) [![Netlify Status](https://api.netlify.com/api/v1/badges/d48eb564-2289-400b-b2d5-3ef225ed00a6/deploy-status)](https://app.netlify.com/sites/tdp/deploys)
# Pei
Website for Think Differently, code-named Pei, which is the Greek word πει meaning "to say."